﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;

namespace FolderWatch
{
	class Program
	{
		public static string UploadPath = "";
		public static string WatchPath = ""; // params as comma seperated file extensions
		public static Queue<FileSystemEventArgs> ChangedFiles = new Queue<FileSystemEventArgs>();
		public static List<string> FileFilter = new List<string> { ".*" };
		public static List<string> IgnoreFilter = new List<string>();
		public static NotifyIcon NotificationIcon = new NotifyIcon
		{
			Icon = new Icon("C:\\Windows\\System32\\PerfCenterCpl.ico"),
			BalloonTipIcon = ToolTipIcon.Info,
			Visible = true
		};

	    private static bool DeleteDisabled = true;

	    static void Main(string[] args)
		{
			Console.WriteLine("Provided " + args.Length + " arguments");

			ThreadPool.SetMaxThreads(2, 2);
			if (args.Length < 2)
			{
				Console.WriteLine("Please provide both the watch path and upload path");
				Console.Write("Watch Path:");
				WatchPath = Path.GetFullPath(Console.ReadLine().Trim('"'));
				Console.Write("Upload path:");
				UploadPath = Path.GetFullPath(Console.ReadLine().Trim('"'));
			}
			if (args.Length >= 2)
			{
				UploadPath = Path.GetFullPath(args[1].Trim('"'));
				WatchPath = Path.GetFullPath(args[0].Trim('"'));
			}

			if (args.Length >= 3 && args[2] != "-delete")
			{
				FileFilter = new List<string>(args[2].Split('|'));
			}

	        if (args.Length >= 4 && args[3] != "-delete")
	        {
	            IgnoreFilter = new List<string>(args[3].Split('|'));
	        }
            if (args.Contains("-delete"))
            {
                DeleteDisabled = false;
            }

	        var watcher = new FileSystemWatcher(WatchPath);
			watcher.IncludeSubdirectories = true;
			//TODO Save Session setups
			watcher.EnableRaisingEvents = true;
			watcher.Changed += WatcherChanged;
			//watcher.Created += WatcherChanged;
			//watcher.Renamed += WatcherChanged; //TODO Change to delete old file
			watcher.Deleted += WatcherDeleted;
			StartProcessingChangedFiles();
			while (true)
			{
				Thread.Sleep(1);
			}
		}

		static void DisplayNotification(string notification)
		{
			ThreadPool.QueueUserWorkItem(delegate
			{
				NotificationIcon.Visible = true;
				var balloonTipText = notification;
				var bgColor = Console.BackgroundColor;
				var fgColor = Console.ForegroundColor;
				Console.BackgroundColor = ConsoleColor.Yellow;
				Console.ForegroundColor = ConsoleColor.DarkBlue;
				Console.WriteLine(balloonTipText);
				Console.BackgroundColor = bgColor;
				Console.ForegroundColor = fgColor;
				Console.WriteLine();
				NotificationIcon.BalloonTipText = notification;
				NotificationIcon.BalloonTipTitle = notification;
				NotificationIcon.ShowBalloonTip(200);
				Thread.Sleep(1000);
				NotificationIcon.Visible = false;
			});
		}

		static void WatcherDeleted(object sender, FileSystemEventArgs eventArgs)
		{
		    if (DeleteDisabled)
		    {
                Console.WriteLine("[Function Disabled] Deleted file " + eventArgs.FullPath);
		    }
		    else
		    {
                try
                {
                    File.Delete(UploadPath + eventArgs.FullPath.Replace(WatchPath, ""));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception while trying to delete file: " + eventArgs.FullPath);
                }
		    }
		}

		private static void WatcherChanged(object sender, FileSystemEventArgs e)
		{
			if ((FileFilter.Contains(".*") && !IgnoreFilter.Any(ignore => e.FullPath.Contains(ignore))) || (FileFilter.Contains(Path.GetExtension(e.Name)) && !IgnoreFilter.Any(ignore => e.FullPath.Contains(ignore))))
			{
				if (e.ChangeType == WatcherChangeTypes.Changed)
				{
					ChangedFiles.Enqueue(e);
				}
				if (e.ChangeType == WatcherChangeTypes.Created)
				{
					ChangedFiles.Enqueue(e);
					/*ThreadPool.QueueUserWorkItem(delegate(object eventArgs)
													 {
														 Thread.Sleep(50);
														 var eventargs = (FileSystemEventArgs)eventArgs;
														 Console.WriteLine("Uploading new file " + eventargs.FullPath);
														 try
														 {
															 File.Copy(eventargs.FullPath, UploadPath + Path.DirectorySeparatorChar + Path.GetFileName(eventargs.Name), true);
														 }
														 catch
														 {
															 Console.WriteLine("New Mittens!");
														 }
														 Console.WriteLine("New file uploaded");
													 }, e);*/
				}
			}
		}

		public static void StartProcessingChangedFiles()
		{
			ThreadPool.QueueUserWorkItem(delegate
			{
				while (true)
				{
					Thread.Sleep(1);
				    if (ChangedFiles.Count > 0)
				    {
				        var eventArgs = ChangedFiles.Dequeue();
				        lock (ChangedFiles)
				        {
				            while (ChangedFiles.Any() && ChangedFiles.Peek().FullPath == eventArgs.FullPath)
				            {
				                Console.WriteLine("Duplicate file added to changed files, removing current queue count: " +
				                                  ChangedFiles.Count);
				                ChangedFiles.Dequeue();
				            }
				            ThreadPool.QueueUserWorkItem(delegate(object state)
				            {
				                var fileEventArgs = (FileSystemEventArgs) state;
				                Console.WriteLine("Uploading changed file " + fileEventArgs.FullPath);
				                try
				                {
				                    Console.WriteLine(DateTime.Now + " " + UploadPath +
				                                      fileEventArgs.FullPath.Replace(WatchPath, ""));
				                    File.Copy(fileEventArgs.FullPath, UploadPath + fileEventArgs.FullPath.Replace(WatchPath, ""),
				                        true);
				                }
				                catch (Exception e)
				                {
				                    if (e is DirectoryNotFoundException)
				                    {
				                        Console.WriteLine("Directory does not exist at destination, creating..");
				                        if (fileEventArgs.FullPath != null)
				                        {
				                            var endPath =
				                                Path.GetDirectoryName(UploadPath + fileEventArgs.FullPath.Replace(WatchPath, ""));
				                            if (endPath != null)
				                            {
				                                Directory.CreateDirectory(endPath);
				                            }
				                            else
				                            {
				                                Console.WriteLine("End path was null..");
				                            }
				                        }
				                        else
				                        {
				                            Console.WriteLine("Full path not found...");
				                        }
				                        ChangedFiles.Enqueue(fileEventArgs);
				                    }
				                    else
				                    {
				                        Console.WriteLine("Changed Mittens!");
				                    }
				                }
				                DisplayNotification(DateTime.Now + " Changed file uploaded " + fileEventArgs.Name);
				            }, eventArgs);
				        }
				    }
				}
			});
		}
	}
}
